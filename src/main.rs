use clap::{App, Arg};
use image::Luma;
use qrcode::QrCode;

fn main() {
    let matches = App::new("")
        .version("0.1.0") // TODO: Pull this value from the Cargo.toml
        .author("Dawson R")
        .about("Generate QR codes from command line!")
        .arg(
            Arg::with_name("out")
                .short("o")
                .long("out")
                .takes_value(true)
                .help("Output file to write"),
        )
        .arg(
            Arg::with_name("INPUT")
                .help("Data to encode into a QR code")
                .required(true)
                .index(1),
        )
        .get_matches();
    let output_file = match matches.value_of("out") {
        Some(v) => v,
        None => "qrcode.png",
    };

    let input = match matches.value_of("INPUT") {
        Some(v) => v.as_bytes(),
        None => panic!("An input value is required to encode into a QR code"),
    };

    if input.len() > 32 {
        panic!("The input must be sufficiently small (less than 32 bytes) to encode as a QR code. Consider using a link shortener to shorten your URL");
    }
    let code = QrCode::new(input).unwrap();

    // Render the bits into an image.
    let image = code.render::<Luma<u8>>().build();

    // Save the image.
    image.save(output_file).unwrap();
}
